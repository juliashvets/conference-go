import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    url = f"https://api.pexels.com/v1/search?query={city} {state}"

    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict['photos'][0]['src']['original']

    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    geo_response = requests.get(geo_url)
    geo_data = geo_response.json()
    lat = geo_data[0]['lat']
    lon = geo_data[0]['lon']

    # Create the URL for the current weather API with the latitude and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put them in a dictionary
    # Return the dictionary

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    weather_response = requests.get(weather_url)
    weather_data = weather_response.json()
    temperature = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]
    weather = {
        "temp": temperature,
        "description": description
    }
    return weather
